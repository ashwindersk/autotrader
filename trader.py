from datetime import datetime, timedelta
import logging
import requests
import json
import os
import csv
import get_recommendations as rec
import trade_creator as trade

from flask import Flask
from flask_restx import Resource, Api

LOG = logging.getLogger(__name__)


# Enviroment variables
os.environ['PORTFOLIO_SERVICE'] = "http://portfolio-manager.citiemealinux5.conygre.com/"
PORTFOLIO = os.environ.get('PORTFOLIO_SERVICE')


# Get portfolio
def get_portfolio():
    autotraders_id = "002"
    requestUrl = PORTFOLIO + "v1/portfolio/" + autotraders_id
    response = requests.get(requestUrl)
    if (response.status_code == 200):
        LOG.debug("Request OK")
        json_data = response.text
        portfolio = json.loads(json_data)
        return portfolio
    else:
        LOG.error("Resquest issue: " + str(response.status_code))

# Post List of Trades to Portfolio Manager.
def do_trades(portfolio, date):
    cash = portfolio['cash']
    recommendations = rec.get_recommendations(date)
    daily_update = dict()
    daily_trades = dict()
    
    # Example : { “AAPL”  : { “Price”: 500, “Action”: 1}, }
    for recommendation in recommendations:
        
        # Sells
        if (recommendation.get("Action") == -1):
            trade.make_trade(recommendation, recommendation.get("Price"), 1, "SELL")
            # Add to Dictionary of today trades
            daily_trades[recommendation] = {
                "Price" : recommendation.get("Price"),
                "Action": -1
                }
            # Update budget
            cash += recommendation.get("Price")

        # Buys
        if (recommendation.get("Action") == 1):
            # Check if you can afford
            if ( cash > recommendation.get("Price")):
                cash -= recommendation.get("Price")
                trade.make_trade(recommendation, recommendation.get("Price"), 1, "BUY")
            # Add to Dictionary of today trades
            daily_trades[recommendation] = {
                "Price" : recommendation.get("Price"),
                "Action": 1
                }


    # Example for testing trades Uncomment as needed
    # appl = recommendations[1]
    # trade.make_trade("AAPL", appl['AAPL'].get("price"), 1, "SELL")
    # daily_trades['AAPL'] = {
    #             "Price" : appl['AAPL'].get("price"),
    #             "Action": 1
    #             }
    # tsla = recommendations[0]
    # daily_trades['TSLA'] = {
    #             "Price" : tsla['TSLA'].get("price"),
    #             "Action": -1
    #             }

    # Build Object
    daily_update['user_id'] = "002"
    daily_update['trades'] = daily_trades

    return daily_update

# Pass back to portfolio Manager
def end_day(update):
    requestURL = PORTFOLIO + "v1/portfolio/"
    request = update
    LOG.info(request)
    response = requests.post(requestURL, json=request)
    LOG.debug("Request status code:", str(response.status_code))
    LOG.info("Closeing trade for day.")