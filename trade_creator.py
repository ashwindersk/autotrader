from datetime import datetime, timedelta
import logging
import requests
import json
import os
import csv
import get_recommendations as rec

from flask import Flask
from flask_restx import Resource, Api

LOG = logging.getLogger(__name__)


# Enviroment variables
os.environ['TRADE_SERVICE'] = "http://trading-platform.citiemealinux5.conygre.com/"
TRADE = os.getenv('TRADE_SERVICE')

# Post Trade to JAVA sercvice
def make_trade(stock, price, number, type):
    trade = {
        "ticker": stock,
        "price": price,
        "stockQuantity": number,
        "type": type,
        "userId": "002",
        "createdDate": datetime.now().strftime("%Y-%m-%d")
        }
    
    requestUrl = TRADE + "v1/trade/"
    requestbody = trade
    response = requests.post(requestUrl, json=requestbody)
    LOG.debug("trade resonse: " + str(response.status_code))