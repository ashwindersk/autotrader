from datetime import datetime, timedelta
import logging
import requests
import json
import os
import csv

from flask import Flask
from flask_restx import Resource, Api

LOG = logging.getLogger(__name__)


# Enviroment variables
os.environ['RECOMMENDATION_SERVICE'] = "http://recommendation-machine.citiemealinux5.conygre.com/"
os.environ['EXCHANGE_LIST'] = "default.csv"
RECOMMEND = os.environ.get('RECOMMENDATION_SERVICE')
EXCHANGE = os.environ.get('EXCHANGE_LIST')

# Get a list of stocks
def get_stocks():
    # At some point this will get a list like FTSE etc
    # stocks = ['TSLA',  'AAPL', 'GOOGL', 'MSFT', 'NFLX']
    stocks = []
    with open(EXCHANGE, newline='') as csvfile:
        
        stockreader = csv.reader(csvfile)
        next(stockreader)
        for row in stockreader:
            stocks.append(row[0])
    LOG.info(stocks)
    return stocks


# Make GET request to get recommendations for stocks
def get_recommendations(date):
    
    # Make Request
    stocks = get_stocks()
    LOG.debug("Getting recommendation request for " + date)
    request = {
        "tickers" : stocks,
        "date": date
    }
    requestURL = RECOMMEND + "v1/recommendation/"
    response = requests.post(requestURL, json=request )
    LOG.debug("Request: " + str(response.request.body))
    if (response.status_code == 200):
        LOG.debug("Request OK")
        LOG.debug("Return from request: " + str(response.content))
        # Decode Response from JSON
        # recommendations = json.load(response.text)
        json_data = response.text
        recommendations = json.loads(json_data)
        return recommendations
    else:
        LOG.error("Resquest issue: " + str(response.status_code))