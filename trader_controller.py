from datetime import datetime, timedelta
import logging
import requests
import json
import trader
import flask_cors

from flask import Flask
from flask_restx import Resource, Api, fields

LOG = logging.getLogger(__name__)
app = Flask(__name__)
api = Api(app)
flask_cors.CORS(app, resources=r'/*')

ns = api.namespace('v1/autotrade/', description='Trade Automatically')

portfolio = api.model('portfolio', {
            'Ticker': fields.Float(required=True),
            # 'Quantity': fields.Float(required=True)
        })

trade_portfolio = api.model('Portfolio', 
    {
        'date' : fields.Date(required=True)
    })

#'portfolio': fields.Nested(ns.model('portfolio', portfolio)) 

@ns.route('/')
class Trade(Resource):

    @ns.expect(trade_portfolio)
    def post(self):
        LOG.info('Beginning training for: ' + api.payload['date'])
     
        # Use Services
        portfolio = trader.get_portfolio()
        date = api.payload['date']
        todays_trades = trader.do_trades(portfolio, date)
        LOG.debug("Todays trades: " + str(todays_trades))
        trader.end_day(todays_trades)

        # Return success status code.
        return json.dumps({'success':True}), 200, {'ContentType':'application/json'}


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    app.run(debug=True, host='0.0.0.0')
